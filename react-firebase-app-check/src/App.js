import logo from './logo.svg';
import './App.css';
import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
import { collection, getDocs, doc, setDoc, addDoc } from "firebase/firestore";



function App() {

  const firebaseConfig = {
    apiKey: "AIzaSyAMfZ_zhSr9Ylkt-uvZuVf0d3yJbwn3VCE",
    authDomain: "fir-appcheck-e0475.firebaseapp.com",
    projectId: "fir-appcheck-e0475",
    storageBucket: "fir-appcheck-e0475.appspot.com",
    messagingSenderId: "347428003102",
    appId: "1:347428003102:web:fa79485bcc89e8daa07d0f"
  };

  const app = initializeApp(firebaseConfig);
  const db = getFirestore(app);

  const incluirLivro = async (event) => {
    console.log('Inclue livro');
    //gravarLivroDB();
    // console.log(db);
    // await setDoc(doc(db, "books"), {
    //   id: 1,
    //   nome: 'A Lua',
    // });

    const docRef = await addDoc(collection(db, "cities"), {
      name: "Tokyo",
      country: "Japan"
    });


    await setDoc(doc(db, "cities", "LA"), {
      name: "Los Angeles",
      state: "CA",
      country: "USA"
    });
  }

  incluirLivro();


  // Initialize Cloud Firestore and get a reference to the service




  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
